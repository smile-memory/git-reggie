package com.smile.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.smile.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

/**
 * dao层接口
 */
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {

}
