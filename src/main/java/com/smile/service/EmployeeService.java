package com.smile.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.smile.entity.Employee;

/**
 * 服务层接口
 */

public interface EmployeeService extends IService<Employee> {

}
