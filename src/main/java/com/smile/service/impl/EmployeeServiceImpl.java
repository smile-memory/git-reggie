package com.smile.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.smile.entity.Employee;
import com.smile.mapper.EmployeeMapper;
import com.smile.service.EmployeeService;
import org.springframework.stereotype.Service;

/**
 * 服务层实现类
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements
        EmployeeService {

}
